import UIKit
import Foundation
import CoreLocation
import CoreMotion

class GPSTrackerManager: NSObject, CLLocationManagerDelegate      {
    
    var speedUnits: [String] = ["ft/s", "km/h", "ko", "mph",  "m/s"]
    var locationManager: CLLocationManager!
    var seenError : Bool = false
    var topSpeed:Double = 0
    let manager = CMMotionManager()

    
    func startTracking() {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
       // locationManager.requestLocation()
        locationManager.startUpdatingLocation()
        locationManager.startUpdatingHeading()
        
       // let _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.checkDirection), userInfo: nil, repeats: true)
    }
    
    func getMotionValues(){
        if manager.isDeviceMotionAvailable {
            manager.deviceMotionUpdateInterval = 0.02
            manager.startDeviceMotionUpdates(to: .main) {
                [weak self] (data: CMDeviceMotion?, error: Error?) in
                
            }
        }
    }
    
    @objc func checkDirection(){
        locationManager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus){
        
        }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.startUpdatingLocation()
        print(error)
        }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        
        let latitude:String = String(format: "%.7f", coord.latitude)
        UserDefaults.standard.set(latitude, forKey: "Latitude")
        let longitude:String = String(format: "%.7f", coord.longitude)
        UserDefaults.standard.set(longitude, forKey: "Longitude")
        
        
        var speed:UInt = 0
        var convertedSpeed:Double = 0
        
        let speedUnit = speedUnits[1]
        let rawSpeed:Double = locations[0].speed
        let accuracy: Double = locations[0].horizontalAccuracy
        
        if accuracy > 0 {
            convertedSpeed = self.getConvertedSpeed(speed: rawSpeed, unit: speedUnit)
            speed = UInt(round(convertedSpeed))
            let speed1:Double = rawSpeed*3.6
            let sSpeed:String = String(format: "%d",speed)
            UserDefaults.standard.set(speed, forKey: "speed")
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {

    }
    
    func getLatitude()->String{
        var sReturnString:String = ""
        if let sLat:String = UserDefaults.standard.value(forKey: "Latitude") as? String{
            sReturnString = sLat
        }
        return sReturnString
    }
    
    func getLongitude()->String{
        var sReturnString:String = ""
        if let sLong:String = UserDefaults.standard.value(forKey: "Longitude") as? String{
            sReturnString = sLong
        }
        return sReturnString
    }
    
    func getSpeed()->Int{
        var sReturnString:Int = 0
        if let sLong:Int = UserDefaults.standard.value(forKey: "speed") as? Int{
            sReturnString = sLong
        }
        return sReturnString
    }
    
    
    func getConvertedSpeed(speed:Double, unit:String)->Double{
        
        var convertedSpeed: Double = 0
        
        if speed < 0 {
            return convertedSpeed;
        }
        
        switch (unit) {
        case "ft/s":
            convertedSpeed = speed * 3.281
        case "km/h":
            convertedSpeed = speed * 3.6
        case "ko":
            convertedSpeed = speed * 1.944
        case "mph":
            convertedSpeed = speed * 2.237
        case "m/s":
            convertedSpeed = speed
        default:
            convertedSpeed = 0
        }
        
        return convertedSpeed;
        
    }
    
}
