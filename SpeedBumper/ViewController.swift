import UIKit
import CoreLocation
import CoreMotion
import GoogleMaps
import GooglePlaces
import AFNetworking
import NVActivityIndicatorView
import SSLocalNotification
import AVFoundation
let manager = CMMotionManager()


class ViewController: UIViewController {

    var labelDirection = UILabel()
    var labelSpeed = UILabel()
    
    var zoomLevel: Float = 17
    var mapView:GMSMapView!
    var activity:NVActivityIndicatorView!
    var dLastLatitude:Double = 0.0
    var dLastLongitude:Double = 0.0
    var dDistance:Double = 0.0
    
    var dStartLatitude:Double = 0.0
    var dStartLongitute:Double = 0.0
    
    var dUserStartLatitude:Double = 0.0
    var dUserStartLongitute:Double = 0.0
    
    var lLastLocation = CLLocation()
    
    var sDistance = String()
    var sDirection = String()
    var arrayData = NSMutableArray()
    var player: AVAudioPlayer?
    var bIsForSoundPlay = Bool()
     var nSpeedOfVehicle = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
        setLoadingIndicator()
        labelDirection.frame = CGRect(x: 0, y: self.view.frame.height-50, width: self.view.frame.width/2, height: 50)
        labelDirection.textAlignment = .center
        labelDirection.backgroundColor = UIColor.white
        labelDirection.font = UIFont.boldSystemFont(ofSize: 30)
        bIsForSoundPlay = true
        
        labelSpeed.frame = CGRect(x: self.view.frame.width/2, y: self.view.frame.height-50, width: self.view.frame.width/2, height: 50)
        labelSpeed.textAlignment = .center
        labelSpeed.backgroundColor = UIColor.white
        labelSpeed.font = UIFont.boldSystemFont(ofSize: 20)
        let _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.checkDirection), userInfo: nil, repeats: true)
        //getMotionValues()
        
    }
    
    
    func getMotionValues(){
        if manager.isDeviceMotionAvailable {
            manager.deviceMotionUpdateInterval = 0.02
            manager.startDeviceMotionUpdates(to: .main) {
                [weak self] (data: CMDeviceMotion?, error: Error?) in
                let value:Double = (data!.userAcceleration.x)*(data!.userAcceleration.x) +  (data!.userAcceleration.y)*(data!.userAcceleration.y) +  (data!.userAcceleration.z)*(data!.userAcceleration.z)
                if value > 6{
              //      print(value)
                    
                }

                let value1:Double = (data!.magneticField.field.x)*(data!.magneticField.field.x) +  (data!.magneticField.field.y)*(data!.magneticField.field.y) +  (data!.magneticField.field.z)*(data!.magneticField.field.z)
                if value1 > 3{
                    print(value1)
                    
                }

            }
        }
    }
    
    func loadMap(){
        let camera = GMSCameraPosition.camera(withLatitude: Double(GPSTrackerManager().getLatitude())!,
                                              longitude:Double(GPSTrackerManager().getLongitude())!,
                                              zoom: zoomLevel)
        mapView = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height), camera: camera)
        mapView.camera = camera
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        self.view = mapView
        self.view.addSubview(labelDirection)
        self.view.addSubview(labelSpeed)
    }
    
    func callWebservice(){
        dUserStartLatitude = Double(GPSTrackerManager().getLatitude())!
        dUserStartLongitute = Double(GPSTrackerManager().getLongitude())!
        dStartLatitude = Double(GPSTrackerManager().getLatitude())!
        dStartLongitute = Double(GPSTrackerManager().getLongitude())!

        let param = NSMutableDictionary()
        param.setValue(GPSTrackerManager().getLatitude(), forKey: "lattitude")
        param.setValue(GPSTrackerManager().getLongitude(), forKey: "longitude")
        param.setValue("5", forKey: "radious")
        getLocations(params: param)
        
    }
    
    //MARK:- Webservices
    func getLocations(params:NSMutableDictionary)
    {
        startLoading()
        let manager = AFHTTPSessionManager()
        let stringURL:NSString = "http://pluggdd.com/speed_pump_detection/show_nearest_location.php"
        
        manager.post(stringURL as String, parameters: params, progress: nil, success: { (operation, responseObject) -> Void in
            let responseDictionary:NSDictionary = responseObject as! NSDictionary
            if let _:Any = (responseDictionary).value(forKey: "status")
            {
                let strStatus = (responseDictionary).value(forKey: "status")
                var str:NSString = String(format: "%@", strStatus as! CVarArg ) as NSString
                str = str.replacingOccurrences(of: "Optional()", with: "") as NSString
                
                if str == "1"{
                    if let arrayData:NSArray = responseDictionary.value(forKey: "data") as? NSArray{
                        self.arrayData.removeAllObjects()
                        self.mapView.clear()
                        if arrayData.count > 0{
                            self.arrayData.addObjects(from: arrayData as! [Any])
                            for i in 0...self.arrayData.count-1{
                                
                                let strLatitude:NSString = (( self.arrayData[i] as AnyObject).value(forKey: "lattitude") as? NSString)!
                                let strLongitude:NSString = (( self.arrayData[i] as AnyObject).value(forKey: "longitude") as? NSString)!
                                let stringAddress:String = (( self.arrayData[i] as AnyObject).value(forKey: "address") as? String)!
                                
                                let marker = GMSMarker()
                                marker.position = CLLocationCoordinate2D(latitude: strLatitude.doubleValue, longitude: strLongitude.doubleValue)
                                marker.title = stringAddress
                                marker.appearAnimation = .pop
                                if let sStatus:String = ( self.arrayData[i] as AnyObject).value(forKey: "status") as? String{
                                    if sStatus == "0"{
                                        marker.icon = UIImage(named: "speed_breaker_sign")
                                    }else if sStatus == "1"{
                                        marker.icon = UIImage(named: "pothole")
                                    }else {
                                        marker.icon = UIImage(named: "road_work")
                                    }
                                }
                                marker.map = self.mapView
                                
                            }
                        }
                    }
                }else{
                    let strStatus:String = (responseDictionary).value(forKey: "msg") as! String
                    self.showAlert(TitleHeader: "Information", MessageContent: strStatus)
                }
            }
            
            self.stopLoading()
        }, failure: { (operation, error) -> Void in
            self.stopLoading()
        })
    }
    
    //MARK:- Activity Indicator View
    func setLoadingIndicator()
    {
        activity = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        activity.color = UIColor.blue
        activity.type = NVActivityIndicatorType.ballScaleMultiple
        activity.startAnimating()
        activity.center = view.center
    }
    func startLoading()
    {
        view.isUserInteractionEnabled = false
        self.view.addSubview(activity)
    }
    
    func stopLoading(){
        activity.removeFromSuperview()
        self.view.isUserInteractionEnabled = true
    }
    
    @objc func checkDirection(){
        nSpeedOfVehicle = GPSTrackerManager().getSpeed()
        if dLastLatitude == 0.0{
            if GPSTrackerManager().getLatitude() != "" {
                lLastLocation = CLLocation(latitude: Double(GPSTrackerManager().getLatitude())!, longitude: Double(GPSTrackerManager().getLongitude())!)
                dLastLatitude = Double(GPSTrackerManager().getLatitude())!
                dLastLongitude = Double(GPSTrackerManager().getLongitude())!
                loadMap()
                callWebservice()
            }
        }else{
            let dUserTotalDistance:Double = kilometersfromPlace(fromLatitude: dStartLatitude, fromLongitude: dStartLongitute, toLatitude: Double(GPSTrackerManager().getLatitude())!, toLongitude: Double(GPSTrackerManager().getLongitude())!)
   
            let dUserStartTotalDistance:Double = kilometersfromPlace(fromLatitude: dUserStartLatitude, fromLongitude: dUserStartLongitute, toLatitude: Double(GPSTrackerManager().getLatitude())!, toLongitude: Double(GPSTrackerManager().getLongitude())!)

            if dUserStartTotalDistance > 4500{
                callWebservice()
            }
            if nSpeedOfVehicle <= 20{
                    bIsForSoundPlay = false
//                if dUserTotalDistance  >  99{
//                    dStartLatitude = Double(GPSTrackerManager().getLatitude())!
//                    dStartLongitute = Double(GPSTrackerManager().getLongitude())!
//                    bIsForSoundPlay = true
//                }
            }else if nSpeedOfVehicle > 21 && nSpeedOfVehicle < 51{
                if dUserTotalDistance  >  99{
                    dStartLatitude = Double(GPSTrackerManager().getLatitude())!
                    dStartLongitute = Double(GPSTrackerManager().getLongitude())!
                    bIsForSoundPlay = true
                }
            }else{
                if dUserTotalDistance <= 199{
                    dStartLatitude = Double(GPSTrackerManager().getLatitude())!
                    dStartLongitute = Double(GPSTrackerManager().getLongitude())!
                    bIsForSoundPlay = true
                }
            }
            calculateDegrees()
        }
        nSpeedOfVehicle = GPSTrackerManager().getSpeed()

        self.labelSpeed.text = String(format:"%d Km/Hr",nSpeedOfVehicle)
        
    }
    
    func calculateDegrees(){
        let dNewDistance:Double = kilometersfromPlace(fromLatitude: dLastLatitude, fromLongitude: dLastLongitude, toLatitude: Double(GPSTrackerManager().getLatitude())!, toLongitude: Double(GPSTrackerManager().getLongitude())!)
        
        if dNewDistance >= 5{
            var dDegree:Double = getBearingBetweenTwoPoints1(point1: lLastLocation, point2: CLLocation(latitude: Double(GPSTrackerManager().getLatitude())!, longitude: Double(GPSTrackerManager().getLongitude())!))
            
            if dDegree == 0.0{
                return
            }
            
            if dDegree < 0.0{
                dDegree = -dDegree
                dDegree = dDegree + 180
            }
            
            if (dDegree >= 315 && dDegree <= 360) || (dDegree >= 0 && dDegree < 45){
                sDirection = "N"
            }else if dDegree >= 45 && dDegree < 135{
                sDirection = "E"
            }else if dDegree >= 135 && dDegree < 225{
                sDirection = "S"
            }else if dDegree >= 225 && dDegree < 315{
                sDirection = "W"
            }
            
            labelDirection.text = sDirection
            lLastLocation = CLLocation(latitude: Double(GPSTrackerManager().getLatitude())!, longitude: Double(GPSTrackerManager().getLongitude())!)
            dLastLatitude = Double(GPSTrackerManager().getLatitude())!
            dLastLongitude = Double(GPSTrackerManager().getLongitude())!
            
            let camera = GMSCameraPosition.camera(withLatitude: Double(GPSTrackerManager().getLatitude())!,
                                                  longitude:Double(GPSTrackerManager().getLongitude())!,
                                                  zoom: zoomLevel)
            mapView.camera = camera
            
            if arrayData.count > 0{
                var nSpeedBraker:Int = 0
                var nPotHole:Int = 0
                var nBadRoad:Int = 0
                var notification:SSLocalNotificationController!
                var bIsShownotification:Bool = false
                for i in 0...arrayData.count-1{
                    
                    let sLatitude:String = (( self.arrayData[i] as AnyObject).value(forKey: "lattitude") as? String)!
                    let sLongitude:String = (( self.arrayData[i] as AnyObject).value(forKey: "longitude") as? String)!
                    
                    let distance:Double = kilometersfromPlace(fromLatitude: dLastLatitude, fromLongitude: dLastLongitude, toLatitude: Double(sLatitude)!, toLongitude: Double(sLongitude)!)
                    
                    if distance <= 100.0{
                        let sDirection1:String = (( self.arrayData[i] as AnyObject).value(forKey: "direction") as? String)!
                        let sStatus:String = (( self.arrayData[i] as AnyObject).value(forKey: "status") as? String)!
                        if sDirection == sDirection1{
                            bIsShownotification = true
                            if sStatus == "0"{
                                nSpeedBraker = nSpeedBraker + 1
                                notification = SSLocalNotificationController(title: "Notification", message: "You are heading speedbump!!!", preferredStyle: .light)
                                notification.image = UIImage(named: "speed_breaker_sign")!
                            }else if sStatus == "1"{
                                nPotHole = nPotHole + 1
                                notification = SSLocalNotificationController(title: "Notification", message: "You are heading a pothole!!!", preferredStyle: .light)
                                notification.image = UIImage(named: "pothole")!
                            }else{
                               nBadRoad = nBadRoad + 1
                                notification = SSLocalNotificationController(title: "Notification", message: "You are heading Man in Work!!!", preferredStyle: .light)
                                notification.image = UIImage(named: "road_work")!
                            }
                        }
                    }
                }
                
                if bIsForSoundPlay == true{
                    if bIsShownotification{
                        notification.setTitleFont(fontName: "Avenir-Medium", color: .black)
                        notification.setMessageFont(fontName: "Avenir-Book", color: .black)
                        notification.presentLocalNotification()
                    }
                    if nSpeedBraker >= 1 && nPotHole >= 1 {
                        self.playSound(songTitle: "bad_road")
                    }else if nSpeedBraker == 1{
                        self.playSound(songTitle: "speed_breaker")
                    }else if nSpeedBraker > 1{
                        self.playSound(songTitle: "multiple_speed_breakers")
                    }else if nPotHole == 1{
                        self.playSound(songTitle: "potholes")
                    }else if nPotHole > 1{
                        self.playSound(songTitle: "multiple_potholes_breakers")
                    }else if nBadRoad > 0 {
                        self.playSound(songTitle: "bad_road")
                    }
                    
                    bIsForSoundPlay = false
                }
            }
        }
    }
    
    func kilometersfromPlace(fromLatitude: Double,fromLongitude: Double, toLatitude: Double,toLongitude: Double) -> Double {
        let userloc = CLLocation(latitude: fromLatitude, longitude: fromLongitude)
        let dest = CLLocation(latitude: toLatitude, longitude: toLongitude)
        let dist:CLLocationDistance = (userloc.distance(from: dest))
        let distance = "\(dist)"
        dDistance = Double(distance)!
        sDistance = NSString(format: "Distance: %.0f", dDistance) as String
        return dDistance
    }
    
    
    func degreesToRadians(degrees: Double) -> Double { return degrees * .pi / 180.0 }
    func radiansToDegrees(radians: Double) -> Double { return radians * 180.0 / .pi }
    
    func getBearingBetweenTwoPoints1(point1 : CLLocation, point2 : CLLocation) -> Double {
        
        let lat1 = degreesToRadians(degrees: point1.coordinate.latitude)
        let lon1 = degreesToRadians(degrees: point1.coordinate.longitude)
        
        let lat2 = degreesToRadians(degrees: point2.coordinate.latitude)
        let lon2 = degreesToRadians(degrees: point2.coordinate.longitude)
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
        
        return radiansToDegrees(radians: radiansBearing)
    }
    
    //MARK:- POPUP ALERT
    func showAlert(TitleHeader:String,MessageContent:String){
        let alert = UIAlertController(title: TitleHeader, message: MessageContent, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    //MARK:- Play Sound
    func playSound(songTitle:String) {
        let url = Bundle.main.url(forResource: songTitle, withExtension: "mp3")!
        do {
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            player.prepareToPlay()
            player.volume = 1.0
            player.play()
        } catch let error as NSError {
            print(error.description)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        player?.stop()
    }
    
}


